<?php

/**
 * @version
 *
 * @developers:
 *    Rafal Wieczorek <kenorb@gmail.com>
 */

/**
 * Define list of functions which custom flags
 */
function magic_cache_load_rules() {
  global $mc_function_hooks, $mc_global_config;
  // get default values
  $mc_function_hooks = magic_cache_variable_get('mc_function_hooks', array());
  $mc_global_config = magic_cache_variable_get('mc_global_config', array());

  /* NON-CACHEABLE FUNCTION */
  /*
  //$mc_function_hooks['user_login_block']->flags |= MC_NO_CACHE; // #action could contain some dynamic variables
  //$mc_function_hooks['system_elements']->flags |= MC_NO_CACHE; // $form['#action'] could contain some dynamic variables
  $mc_function_hooks['node_page_default']->flags |= MC_NO_CACHE; // FIXME
  $mc_function_hooks['user_block']->flags |= MC_NO_CACHE; // FIXME (doesn't work during login process)
  $mc_function_hooks['block_list']->flags |= MC_NO_CACHE; // FIXME (doesn't work during login process)
  $mc_function_hooks['user_load']->flags |= MC_NO_CACHE; // FIXME (doesn't work during login process)
  $mc_function_hooks['user_uid_optional_load']->flags |= MC_NO_CACHE; // FIXME
  */
  //$mc_function_hooks['node_form']->flags |= MC_NO_CACHE; // activate on side effects
  //$mc_function_hooks['node_build_content']->flags |= MC_NO_CACHE; // activate on side effects
  
  /* CACHE_KEY CUSTOM CHANGES */
  $mc_function_hooks['node_access_grants']->flags |= MC_BEFORE_CALL; /* set calback */
  $mc_function_hooks['node_access_grants']->callback = 'magic_cache_node_access_grants_callback'; /* set calback */
  
  /* DELAYED EXECUTION */
  /* log logs into database after exit */
  $mc_function_hooks['dblog_watchdog']->flags |= MC_RUN_ON_CRON | MC_RUN_ON_FLUSH; // CHECK ME: should be OR
  $mc_function_hooks['dblog_watchdog']->after = MC_STATE_EXIT;
  $mc_function_hooks['dblog_watchdog']->result = MC_STATE_EXIT;
  
  $mc_function_hooks['system_modules']->flags = MC_RESULT_ALTER;
  $mc_function_hooks['system_modules']->callback = 'magic_cache_cache_rebuild_callback'; /* set function callback */
  
  /* rule for fixing syslog watchdog call on shutdown */
  $mc_function_hooks['syslog_watchdog']->flags |= MC_RUN_ON_CRON | MC_RUN_ON_FLUSH; // CHECK ME: should be OR
  $mc_function_hooks['syslog_watchdog']->after = MC_STATE_EXIT;
  $mc_function_hooks['syslog_watchdog']->result = MC_STATE_EXIT;
  /* Why? Bad workflow on shutdown:
   * drupal_error_handler -> watchdog -> module_invoke -> call_user_func_array -> syslog_watchdog -> theme -> init_theme -> _init_theme -> _theme_load_registry -> _theme_build_registry -> _theme_process_registry -> include_once() -> WOW!!!
   * 
   * Note: on shutdown all static variables are cleared, so Drupal on theme() call, trying to rebuild the whole theme;/
   */

  /* special rules for hooks */
  $mc_global_config['combine']->hooks = array( /* combine all specified hooks into one (dosn't work in FILE mode */
    'boot' => TRUE,
    'init' => TRUE,
    'nodeapi' => TRUE,
    'form_alter' => TRUE,
    'footer' => TRUE,
    'exit' => TRUE,
  ); /* TODO: there is not big difference between performance ;( */
  // $mc_global_config['combine']->hooks = array(); // uncomment to disable hook combining
  
  /* ACTIVATE AUTOLEARN MODE */
  $mc_global_config['autolearn']->settings = array(
    'active' => TRUE, // autolearn mode active
    'no' => 2,        // after 2 different results returned by function, stop caching it
    'debug' => FALSE, // turn on additional messages
  );
/* TODO: better cache rules for: block_list */
}

/**
 * MAIN FUNCTION to handle modules by cache engine
 *
 * @param array $module_names
 *   array with module names to activate
 * @param array $files (optional)
 *   which files should be loaded, if not provided - files will be scanned from directory automatically
 * @param constant $replace_method (optional)
 *   select which module should be activated (MC_EVAL_MODE, MC_DYNAMIC_MODE, MC_FILE_MODE)
 * @param string $init_code (optional)
 *   init code before each loaded file
 */
function magic_cache_replace_file($module_names, $files = array(), $replace_method = MC_EVAL_MODE, $init_code = '') {
  include_once './includes/file.inc';
  
  /* convert arguments into array */
  if (is_string($module_names)) {
    $module_names = array($module_names => $module_names);
  }
  
  
  $cache_files = magic_cache_variable_get('cache_files', array());
  foreach ($module_names as $no => $module_name) {
    /* get original module path */
    $org_filepath = magic_cache_get_org_path($module_name);
  
    /* create fake module files and execute the code */
    if (!array_key_exists($module_name, $cache_files) && empty($files)) {
      $module_dir = $cache_files[$module_name]["$module_name.module"]['org'] ? dirname($cache_files[$module_name]["$module_name.module"]['org']) : dirname(drupal_get_filename('module', $module_name));
      $files = file_scan_directory($module_dir, "(.inc|.module|.install)$");
    } else {
      $files = $cache_files[$module_name];
    }
    $module_changes = FALSE;
    foreach ($files as $file) {
      $filename = is_array($file) ? basename($file['org']) : (is_object($file) ? $file->basename : $file); // convert from array or object if necessary
      $filepath = dirname($org_filepath) . '/' . $filename; // get dir path of original file
      $cache_file = $cache_files[$module_name][$filename]['cache'];
      $old_engine_method = $cache_files[$module_name][$filename]['method'];
      $is_module_file = strpos($filename, '.module') !== FALSE ? TRUE : FALSE;
      $newfile = FALSE; // if file was newly loaded, execute additional code
      switch ($replace_method) {
        case MC_DYNAMIC_MODE:
          global $mc_dyn_funcs;
          if (empty($mc_dyn_funcs)) {
            $mc_dyn_funcs = cache_get(MC_CID_DYN_FUNC, MC_CACHE_TABLE)->data; // load dynamic function definitions from cache
            if (empty($mc_dyn_funcs)) {
              unset($cache_file);
            }
          }
        case MC_EVAL_MODE:
          global $mc_code_blocks;
          $key = $filename;
          if ($create_file = !file_exists($cache_file)
            || !isset($mc_code_blocks->$key) 
            || empty($cache_file) 
            || $old_engine_method <> $replace_method)
          { // check if code is not loaded yet
            $new_code = file_exists(realpath($filepath)) ? $init_code . file_get_contents(realpath($filepath)) : '';
            magic_cache_new_eval_code($new_code, $key, $replace_method, $module_name);
            $newfile = TRUE;
          }
          $new_filepath = $create_file || $old_engine_method <> $replace_method ? magic_cache_create_fake_module($filename, $module_name, ($replace_method == MC_FILE_MODE ? $code : NULL)) : $cache_files[$module_name][$filename]['cache'];
          //require_once $new_filepath;
          //if (!isset($_SESSION['_mc_code'][$filename])) {          }
          break;
        case MC_FILE_MODE:
          if (empty($cache_file) || !file_exists($cache_file) || $old_engine_method <> $replace_method) { // check if already exists
            magic_cache_unload_block_code($filename);
            $code = file_exists(realpath($filepath)) ? $init_code . file_get_contents(realpath($filepath)) : '';
            $code = magic_cache_replace_functions($code, $replace_method, $module_name); // replace function on the fly
            $new_filepath = magic_cache_create_fake_module($filename, $module_name, ($replace_method == MC_FILE_MODE ? $code : NULL));
            $newfile = TRUE;
          } else if (!empty($cache_file)) {
            $new_filepath = $cache_file;
          }
          //if ($is_module_file) { // only main module will be loaded
            include_once $new_filepath; // include our file with rewritten functions
            /* later path of included files will be updated, but now we have to load them all to not make any function conflict */
          //}
          break;
        case 'cache':
        default:
            drupal_set_message('Cache: Unsupported cache engine mode!', 'error'); // Note: t() is not accessible during hook_boot
          break;
      }
      if ($newfile) {
        $cache_files[$module_name][$filename]['cache'] = $new_filepath;
        $cache_files[$module_name][$filename]['method'] = $replace_method;
        $new_filepath <> $filepath ? $cache_files[$module_name][$filename]['org'] = $filepath : NULL; // set original path only if it's different than cache (FIXME)
        db_query("UPDATE {menu_router} SET file = '%s' WHERE file = '%s'", $new_filepath, $filepath); // update dependencies in menu_router
        $module_changes = TRUE;
      }
    }
    if ($module_changes) {
      //$old_engine_method == MC_FILE_MODE ? magic_cache_remove_cache_file($module_name) : NULL; // FIXME: remove module files
      magic_cache_update_system_path($module_name, $cache_files); // update fake module path in system table
    }
    $files = array(); // remove loaded files from variable, so later it can be used again
    /* update new path */

    magic_cache_variable_set('cache_files', array_unique($cache_files)); // update cache file list
    drupal_get_filename('module', $module_name, $new_filepath); // set new path into static variable
    $replace_method == MC_EVAL_MODE || $replace_method == MC_DYNAMIC_MODE ? magic_cache_new_eval_code('', NULL, $replace_method, $module_name) : NULL; // execute the code, if it's eval
  }
  if ($mc_dyn_funcs[0]['refresh']) { // save definitions of dynamic function to cache if necessary
    $mc_dyn_funcs[0]['refresh'] = FALSE;
    cache_set(MC_CID_DYN_FUNC, $mc_dyn_funcs, MC_CACHE_TABLE); // FIXME: later it could merge with existing list
  }
  /* at the end - execute combined hooks if configured */
  global $mc_global_config;
  if (!empty($mc_global_config['combine']->hooks) && $replace_method !== MC_FILE_MODE) {
    magic_cache_eval_hooks();
  }
}

/**
 * Get original path of the module
 *
 * @param string $module_name
 *   module name
 */
function magic_cache_get_org_path($module_name) {
  $org_filepaths = magic_cache_variable_get('org_paths', array());
  if (!array_key_exists($module_name, $org_filepaths)) {
    $org_filepaths[$module_name] = drupal_get_filename('module', $module_name);
    magic_cache_variable_set('org_paths', $org_filepaths);
  }
  return $org_filepaths[$module_name];
}

/**
 * Create fake file with specified code
 *
 * @param string $file
 *   filename
 * @param string $module_name
 *   module name
 * @param string $code
 *   code which will be written into created file
 */
function magic_cache_create_fake_module($file, $module_name, $code) {
  $cache_dir = magic_cache_variable_get('cache_dir', file_directory_path() . '/' . 'magic_cache');
  $dir = $cache_dir . '/' . $module_name;
  $fake_file = "$dir/$file";
  if (is_dir($dir) || @mkdir($dir, 0755, TRUE)) { // create dir if necessary
      if ($fp = fopen($fake_file, 'w')) {
        fwrite($fp, $code ? $code : "<?php\r\n");
        fclose($fp);
      }  else {
        watchdog('magic_cache', 'Unable to create file: %opt_file', array('%opt_file' => $fake_file), array(), WATCHDOG_WARNING);
      }
  } else {
    watchdog('magic_cache', 'Unable to create dir: %dir', array('%dir' => $dir), array(), WATCHDOG_WARNING);
  }
  return $fake_file;
}

/**
 * Add new eval code, which will be executed
 *
 * @param string $code
 *   input PHP code to save
 * @param string $key
 *   key of code, if NULL then execute all block which has been added before
 * @param constant $replace_method (optional)
 *   select which module should be activated (MC_EVAL_MODE, MC_DYNAMIC_MODE, MC_FILE_MODE)
 * @param string $module_name (optional)
 *   module name
 */
function magic_cache_new_eval_code($code = '', $key = NULL, $replace_method = MC_EVAL_MODE, $module_name = NULL) {
  global $mc_code_blocks;
  static $new_code = array(), $in_memory = array();
  if (!empty($key)) {
    $new_code[$key] = str_replace('<?php', '', $code); // FIXME?
  } else { // if key is empty, execute the whole code
    if (!empty($new_code)) {
      foreach ($new_code as $code_key => &$code_block) {
        if (!array_key_exists($code_key, $in_memory)) { // don't parse code which is already in memory
          $mc_code_blocks->$code_key = magic_cache_replace_functions($code_block, $replace_method, $module_name); // replace function on the fly
        }
      }
      $cache_data = serialize($mc_code_blocks);
      if (magic_cache_check_db_max_allowed_packet(round(strlen($cache_data)/1024/1024))) {
        cache_set(MC_CID_CODE, $cache_data, MC_CACHE_TABLE); // save block codes into cache
      } else {
        cache_clear_all(MC_CID_CODE, MC_CACHE_TABLE); // remove block codes from cache
      }
    }
    if (is_object($mc_code_blocks)) {
      foreach ($mc_code_blocks as $code_key => &$code_block) {
        if ($code_key != 'hooks' && !array_key_exists($code_key, $in_memory)) {
          ob_start();
          eval($code_block);
          ob_end_clean();
          $in_memory[$code_key] = TRUE;
        }
      }
    }
  }
}

/**
 * Execute combined hooks
 *
 */
function magic_cache_eval_hooks() {
  global $mc_code_blocks;
  static $in_memory = array();
  foreach ($mc_code_blocks->hooks as $hook_name => $code_block) {
    if (!array_key_exists($hook_name, $in_memory)) {
      $code = implode('', $code_block);
      ob_start();
      eval($code);
      ob_end_clean();
      $in_memory[$hook_name] = TRUE;
    }
  }
}

/**
 * Revert loaded module and remove temporary files and directory
 *
 * @param string $module_name
 *   name of the module to revert
 */
function magic_cache_revert_module($module_name = '*') {
  $cache_files = magic_cache_variable_get('cache_files', array());
  foreach ($cache_files as $module_loaded => $module_files) {
    if ($module_name == '*' || $module_loaded == $module_name) {
      foreach ($module_files as $filename => $filepath) {
        db_query("UPDATE {menu_router} SET file = '%s' WHERE file = '%s'", $filepath['org'], $filepath['cache']); // revert dependencies in menu_router
        if (strpos($filepath['org'], '.module') !== FALSE) { // update in system only module files
          db_query("UPDATE {system} SET filename = '%s' WHERE name = '%s'", $filepath['org'], $module_loaded);
        }
      }
      magic_cache_remove_cache_file($module_loaded);
    }
  }
  magic_cache_remove_cache_file();
  $other_modules = db_query("SELECT filename FROM {system} WHERE filename LIKE '%%%s%%' AND name != '%s'", 'magic_cache', 'magic_cache');
  if ($other_modules) {
      while ($row = db_fetch_object($other_modules)) {
        $dir = dirname($row->filename);
        if (file_exists($row->filename) && unlink(realpath($row->filename))) {
          drupal_set_message("Cache file successfully removed! File: $row->filename", 'status'); // t() not available during hook_boot
          drupal_set_message("Please remove directory `$dir` manually!", 'error');
        }
      }
  }
  magic_cache_clear_cache(); // clear cache
  cache_clear_all('*', 'cache', TRUE); // especially we are interested with clearing schema and theme_registry to not cause the conflicts
  magic_cache_rebuild_system_table(); // Rebuild system table
  magic_cache_force_module_load_all(); // Load all enabled modules
  magic_cache_rebuild_menu_router_table(); // Rebuild menu_router table
}

/**
 * Rebuild system table
 *
 */
function magic_cache_rebuild_system_table() {
  require_once './includes/file.inc'; // needed for file_scan_directory()
  require_once './modules/system/system.module'; // needed for system_get_files_database()
  module_rebuild_cache(); // rebuild paths in system table
}

/**
 * Force load all enabled modules
 *
 */
function magic_cache_force_module_load_all() {
    foreach (module_list(TRUE, FALSE) as $name) {
      include_once drupal_get_filename('module', $name);
    }
}

/**
 * Rebuild menu_router table
 *
 */
function magic_cache_rebuild_menu_router_table() {
  module_list(TRUE, FALSE); // Refresh the module list to include the new enabled module. [#496198]
  //require_once './includes/theme.inc'; // needed for list_themes()
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL); // we need enter into FULL bootstrap mode to fully rebuild menu_router table
  menu_router_build(TRUE); // Rebuild menu_router table
}
/**
 * Remove loaded module from disc and its directory
 *
 * @param string $module_name
 *   name of the module to revert (if provided '*', then remove them all)
 */
function magic_cache_remove_cache_file($module_name = '*') {
  $cache_files = magic_cache_variable_get('cache_files', array());
  foreach ($cache_files as $module_loaded => $module_files) {
    if ($module_name == '*' || $module_loaded == $module_name) {
      foreach ($module_files as $filename => $filepath) {
        if (file_exists($filepath['cache']) && unlink(realpath($filepath['cache']))) {
          drupal_set_message("Cache file successfully removed! File: {$filepath['cache']}", 'status'); // t() not available during hook_boot
        } else {
          drupal_set_message("Error removing cache file! File: {$filepath['cache']}", 'error'); // t() not available during hook_boot
        }
        unset($cache_files[$module_loaded]);
      }
      if (!rmdir(realpath(dirname($filepath['cache'])))) {
          $dir_name = realpath(dirname($filepath['cache']));
          drupal_set_message("Error removing cache dir! Directory: $dir_name", 'error'); // t() not available during hook_boot
      }
    }
    magic_cache_variable_set('cache_files', $cache_files); // update new cache_files in settings
  }
}

/**
 * Parse and generate function code replacement
 *
 * @param string $content
 *   main code to parse
 * @param constant $replace_method (optional)
 *   select which module should be activated (MC_EVAL_MODE, MC_DYNAMIC_MODE, MC_FILE_MODE)
 * @param string $module_name (optional)
 *   module name
 * @args array $args (optional)
 *   default argument passed to new generated function
 * @args string $gen_func_name (optional)
 *   name of the main general cache function to call each time when original function are called (callback)
 */
function magic_cache_replace_functions($content, $replace_method = MC_EVAL_MODE, $module_name = NULL, $args = array('cache' => MC_CACHE_NORMAL), $gen_func_name = 'magic_cache_generic_call') {
  /* get variables */
  $handle_all_funcs = magic_cache_variable_get('handle_all_funcs', TRUE); // not really difference
  $force_reference = magic_cache_variable_get('force_reference', FALSE); // doesn't work in some cases when called with static strings, needs work
  /* parse code and find the matches */
  preg_match_all("#(?<comment>/\*.*?\*/)?.?(?<func_def>function (?<func_name>[a-z0-9_]+)\((?<func_args>.*?)\)[^)]?{)(?<func_code>.*?)\n}\n\n?#imse", $content, $matches);
  /* define global variables depends on the mode */
  global $mc_global_config;
  switch ($replace_method) {
    case MC_DYNAMIC_MODE:
      global $mc_dyn_funcs; // if dynamic, load existing function definition
    case MC_EVAL_MODE:
      global $mc_code_blocks; // if dynamic or eval mode, then load code_blocks
    break;
  }
  /* create new function using the same names */
  $code = ''; // startup value
  $old_code = ''; // startup value
  foreach ($matches['func_name'] as $key => $func_name) {
    $comment = $matches['comment'][$key]; // get function comment

    /* convert arguments without default values and no references */
    $func_args = &$matches['func_args'][$key];
    $func_code = &$matches['func_code'][$key];
    $force_reference ? $func_args = ltrim(preg_replace('@[^&]\$@', '&$', ' ' . $func_args)) : NULL;
    if (!empty($func_args)) {
      $call_arg = preg_match_all('@(?<func_arg>\$[^,= ]+)@i', $func_args, $arg_matches);
      $call_arg = ', ' . implode(', ', $arg_matches['func_arg']);
    } else {
      $call_arg = '';
    }
    
    /* add custom variables */
    $cache = $args['cache'] ? $args['cache'] : MC_CACHE_NONE; /* cache by default */
    if (strpos($matches['func_code'][$key], 'return ') === FALSE) {
      $cache = MC_CACHE_NONE; // by default disable cache for function, what doesn't return anything
    }
    
    /* check for theme hooks and get registered hooks to use it later for preprocess */
    if ($func_name == "{$module_name}_theme") {
      global $mc_registered_hooks;
      preg_match_all("#'(?<hook_name>$module_name(_[\w]+)?)'#i", $func_code, $hook_matches);
      foreach ($hook_matches['hook_name'] as $key => $hook_name) {
        $mc_registered_hooks[$module_name][$key] = $hook_name;
      }
    }
    if ($replace_method == MC_DYNAMIC_MODE || $replace_method == MC_EVAL_MODE) {
      if (is_array($mc_global_config['combine']->hooks)) {
        foreach ($mc_global_config['combine']->hooks as $hook_name => $settings) {
          if ($func_name == "{$module_name}_${hook_name}") {
            //drupal_set_message($func_name); // TODO
            if (!isset($mc_code_blocks->hooks[$hook_name])) {
              $hook_func_name = "{$module_name}_${hook_name}";
              $mc_code_blocks->hooks[$hook_name][0] = "$comment\nfunction $hook_func_name ($func_args) { \n";
              $mc_code_blocks->hooks[$hook_name][1] = "/* Implementation of {$module_name}_${hook_name} */\n$func_code";
              $mc_code_blocks->hooks[$hook_name][2] = "};\n";
            } else {
              $mc_code_blocks->hooks[$hook_name][1] .= "\n/* Implementation of {$module_name}_${hook_name} */\n$func_code\n";
            }
            continue 2; // next foreach() func_name
          }
        }
      }
    }

    /* define fake function with cache callback function */
    $function_code = '';
    if (!$handle_all_funcs && $cache == MC_CACHE_NONE && empty($call_arg)) { // for testing purposes (disabled by default)
      $function_code = "if (func_num_args() == 0) { return call_user_func('_mc_' . __FUNCTION__); } else "; // ignore cache callback if function returns nothing
    }
    $cache_arg = "'cache' => $cache";
    $method_code = "'method' => $replace_method";
    $args_code = "'args' => func_get_args()";
    $function_code .= "return $gen_func_name(array('function' => __FUNCTION__, $cache_arg, $method_code, $args_code)$call_arg);";
    $code .= "$comment\nfunction $func_name ($func_args) { $function_code }\n";

    /* for dynamic functions */
    if ($replace_method == MC_DYNAMIC_MODE) {
      $mc_dyn_funcs[$func_name]['org_code'] = "function _mc_$func_name ( $func_args ) { $func_code }";
      $mc_dyn_funcs[0]['refresh'] = TRUE;
      $mc_dyn_funcs[$func_name]['args'] = $func_args;
      $mc_dyn_funcs[$func_name]['cache'] = $cache;
      $mc_dyn_funcs[$func_name]['callback'] = '_mc_' . $func_name;
      //$mc_dyn_funcs[$func_name]['run'] = create_function($func_args, $mc_dyn_funcs[$func_name]['org_code']);
    }
  }
  /* create old functions renaming them */
  $old_code = $replace_method !== MC_DYNAMIC_MODE ? preg_replace("/function  ?([_a-z0-9]+)\(/", "function _mc_$1(", $content) : '';
  
  /* save registered hooks */
  if (!empty($mc_registered_hooks)) {
    cache_set(MC_CID_REG_HOOKS, $mc_registered_hooks, MC_CACHE_TABLE); // FIXME: later it could merge with existing list
    cache_clear_all('theme_registry', 'cache', TRUE);
  }
  
  return $old_code . $code;
}

/**
 * Save handler errors into watchdog table
 *
 */
function magic_cache_save_errors() {
  global $mc_error_handler;
  if (empty($mc_error_handler)) {
    return;
  }
  $err_types = array(1 => 'error', 2 => 'warning', 4 => 'parse error', 8 => 'notice', 16 => 'core error', 32 => 'core warning', 64 => 'compile error', 128 => 'compile warning', 256 => 'user error', 512 => 'user warning', 1024 => 'user notice', 2048 => 'strict warning', 4096 => 'recoverable fatal error');
  foreach ($mc_error_handler as $no => $err_arr) {
    //drupal_set_message($err_arr['msg']);
    list($errno, $message, $filename, $line, $context) = $err_arr;
    $entry = $errno .': '. $message .' in '. $filename .' on line '. $line .'.';
    watchdog('php', '%message in %file on line %line.', array('%error' => $types[$errno], '%message' => $message, '%file' => $filename, '%line' => $line), WATCHDOG_ERROR);
    if (strpos($message, 'magic_cache') !== FALSE) {
      drupal_set_message(print_r($err_arr, true));
    }
  }
}

/**
 * Generate cache key - how cache data are stored in cache table
 *
 */
function magic_cache_cache_key() {
  global $user; // global user object
  global $mc_router_item; // save menu_get_item() in globals, because static variables are reset during shutdown, so menu_get_item() will be not cached
  if (!$cache_key = $mc_router_item ? $mc_router_item : menu_get_item()) {
    $cache_key['path'] = 'node';
  }
  $cache_prefix = 'mc:' . $user->uid;
  return $cache_prefix . ':' . $cache_key['path'];
}

/**
 * Check db max_allowed_packet requirements
 * 
 * @param bool min_mb
 *   if TRUE, make additional checking for WSODs (could spent even 3x time)
 * @return string
 *   returns TRUE if db have proper requirements, otherwise FALSE
 * 
 */
function magic_cache_check_db_max_allowed_packet($min_mb = 1) {
  /* check requirements - get db max_allowed_packet */
  global $mc_max_allowed_packet_mb;
  if (!$mc_max_allowed_packet_mb) {
    $mc_max_allowed_packet_mb = magic_cache_get_db_variable('max_allowed_packet')/1024/1024;
  }
  if ($mc_max_allowed_packet_mb <= $min_mb) {
    if (!magic_cache_set_mysql_variable('max_allowed_packet', ($min_mb+1)*1024*1024)) {
      drupal_set_message('Magic Cache require at least 1MB of max_allowed_packet to run, sorry!', 'error');
      $link = "<a href='http://drupal.org/node/379976'>#379976</a>";
      drupal_set_message("Please update your database configuration or disable the module! Read more: $link", 'error');
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * Set MySQL variable
 */
function magic_cache_set_mysql_variable($variable_name, $value, $type = 'SESSION') {
    if (is_numeric($value)) {
        return db_query('SET %s %s = %d', $type, $variable_name, $value);
    } else {
        return db_query('SET %s %s = "%s"', $type, $variable_name, $value);
    }
}

/**
 * Return MySQL variable
 */
function magic_cache_get_db_variable($variable_name, $type = 'SESSION') {
    return db_result(db_query('SELECT @@%s.%s', $type, $variable_name));
}

/* NOT-USED FUNCTION */
function magic_cache_session($op, $key, $value = NULL, $subkey = NULL) {
  $variable = &$_SESSION['magic_cache_data'][$key];
  switch ($op) {
    case 'set':
      !$subkey ? $variable = $value : $variable[$subkey] = $value;
    break;
    case 'del':
      if (!$subkey) {
        unset($variable);
      } else {
        unset($variable[$subkey]);
      }
    break;
    case 'isset':
      return !$subkey ? isset($variable) : isset($variable[$subkey]);
    break;
    case 'get':
      return !$subkey ? $variable : $variable[$subkey];
    break;
  }
}

/**
 * NOT-USED FUNCTION
 * Retrieves the current status of an array of files in the system table.
 *
 * @param $files
 *   An array of files to check.
 * @param $type
 *   The type of the files.
 */
function __mc_system_get_files_database(&$files, $type) {
  // Extract current files from database.
  $result = db_query("SELECT filename, name, type, status, throttle, schema_version FROM {system} WHERE type = '%s'", $type);
  while ($file = db_fetch_object($result)) {
    if (isset($files[$file->name]) && is_object($files[$file->name])) {
      $file->old_filename = $file->filename;
      foreach ($file as $key => $value) {
        if (!isset($files[$file->name]) || !isset($files[$file->name]->$key)) {
          $files[$file->name]->$key = $value;
        }
      }
    }
  }
}

